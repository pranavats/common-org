(asdf:defsystem #:common-org
  :description "Org mode for Common Lisp."
  :author "Pranav Vats <pranavats@disroot.org>"
  :version "0.0.0"
  :license "GPL-3.0"
  :serial t
  :defsystem-depends-on (#:literate-lisp)
  :depends-on (#:maxpc
               #:common-doc
               #:series
               #:serapeum
               #:trivia
               #:with-setf
               #:log4cl)
  :components ((:module :core :pathname "./"
                :components ((:org "common-org"))))
  :in-order-to ((test-op (test-op :common-org/test))))

(asdf:defsystem #:common-org/test
  :defsystem-depends-on (#:literate-lisp)
  :depends-on (#:fiveam
               #:trivia
               #:maxpc
               #:with-setf
               #:common-doc
               #:alexandria
               #:serapeum
               #:closer-mop
               #:anaphora)
  :components ((:org "common-org"
                :around-compile compile-with-tests)))

(defun compile-with-tests (compile-fn)
  (if (member :test *features*)
      (funcall compile-fn)
      (unwind-protect
           (progn (pushnew :test *features*)
                  (funcall compile-fn))
        (setf *features* (remove :test *features*)))))
